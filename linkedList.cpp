#include "linkedList.h"
#include <iostream>
using namespace std;
void initNode(node *head, int data) {
	head->data = data;
	head->next = NULL;
}
void getIn(node **head, int data) {
	node* newNode = new node;
	newNode->data = data;
	newNode->next = *head;
	*head = newNode;
}

void out(node** head)
{
	//if empty list 
	if (*head == NULL)
	{
		cout << "empty";
		return;
	}
	node* newnode = new node;
	newnode = *head;
	*head = newnode->next;
}

void display(node *head) {
	node *list  = head;
	while (list) {
		cout << list->data << " ";
		list = list->next;
	}
	cout << endl;
	cout << endl;
}
