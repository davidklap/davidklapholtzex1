#ifndef STACK
#define STACK
typedef struct node
{
	int data; 
	node* next;
}node ; 
// only for the 1st Node
void initNode(node* head, int data); 
void getIn(node **head, int data);
void out(node** head);
void display(node* head);
#endif /* STACK */

