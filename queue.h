#ifndef QUEUE_H
#define QUEUE_H


/* a queue contains positive integer values. */
typedef struct queue
{
	int* elements; // the arr of the elments  
	unsigned int maxSize; // the mux size of the arry 
	int numOfElements; // counting the num of  the number that in the queue 
	int lastINDEXOutFromQueuePlace; // rember thr last place so  iot wiil not be nessery to to it in o(n) and just o(1)
	int lastIndexIn; // the last place of in num 
} queue;
	
/* fonction crete the queue that impolence with an arr
input: the struct of the queue that include all the data about the strucrt
       and the size of the the queue that the usr ask for 
output : NULL*/
void initQueue(queue* q, unsigned int size);

/* fnction clining the queue 
input : the struct that include all tje data about the struct
output: null 
*/
void cleanQueue(queue* q);
// addd num to the queue
void enqueue(queue* q, unsigned int newValue);
// take out num into the queue
int dequeue(queue* q); // return element in top of queue, or -1 if empty
/*fonction increas all the elments one place forod so the new elment wiil wnter in the forst place */
void increesArr(queue* q);

#endif /* QUEUE_H */